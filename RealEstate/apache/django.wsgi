import os
import sys

path = '/var/www/realestate/RealEstate'
if path not in sys.path:
    sys.path.insert(0, '/var/www/realestate/RealEstate')

os.environ['DJANGO_SETTINGS_MODULE'] = 'RealEstate.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
