from django import forms

class SendSmsForm(forms.Form):
	sender_no = forms.CharField(max_length=1000)
	text = forms.CharField(max_length=1000)


class UserRegistrationForm(forms.Form):
	use_id=forms.HiddenInput()
	first_name = forms.CharField(max_length=100,required=False)
	last_name = forms.CharField(max_length=100,required=False)
 	username = forms.CharField(max_length=100)
	password = forms.CharField(max_length=100)
	password_repeat = forms.CharField(max_length=100)
