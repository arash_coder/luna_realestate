$(function () {
 var validator = new FormValidator(
       'changePassForm',
       [
	   {
               name: 'old_pass',
               display: 'گذرواژه قبلی',
               rules: 'required'
           },
           {
               name: 'new_pass',
               display: 'گذرواژه جدید',
               rules: 'required'
           },
           {
               name: 'repeat_new_pass',
               display: 'تکرار گذرواژه جدید',
               rules: 'required'              
           }
       ],
         function (errors, event) {
             if (errors.length > 0) {
                 event.preventDefault();
                 var result = '<div class="error">خطا در اطلاعات ورودی!!!<ul>';
                 for (var i = 0; i < errors.length; i++) {
                     result += '<li>' + errors[i].message + '</li>';
                 }
                 result += '</ul></div>';
                 $('#changePassFormErrors').html(result);

             }
         }
    );
});
