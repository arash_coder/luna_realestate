$(function () {
 var validator = new FormValidator(
       'userForm',
       [
	   {
               name: 'username',
               display: 'نام کاربری',
               rules: 'required'
           },
           {
               name: 'password',
               display: 'گذرواژه',
               rules: 'required'
           },
           {
               name: 'password_repeat',
               display: 'تکرار گذرواژه',
               rules: 'required'              
           }
       ],
         function (errors, event) {
             if (errors.length > 0) {
                 event.preventDefault();
                 var result = '<div class="error">خطا در اطلاعات ورودی!!!<ul>';
                 for (var i = 0; i < errors.length; i++) {
                     result += '<li>' + errors[i].message + '</li>';
                 }
                 result += '</ul></div>';
                 $('#userFormErrors').html(result);

             }
         }
    );
});
