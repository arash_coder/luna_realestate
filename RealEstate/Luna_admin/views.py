#! /usr/bin/env python
# -*- coding: utf8 -*-

from RealEstate import settings
from django.shortcuts import render,get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, logout, login as auth_login
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponseRedirect
from Luna_admin.models import SmsLog
from Luna_admin.forms import SendSmsForm, UserRegistrationForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied
from django.db import models
from SOAPpy import WSDL

# Create your views here.

def login(request):
    username = password = ''
    if request.POST:
    	username = request.POST.get('username')
        password = request.POST.get('password')
        u = authenticate(username=username, password=password)
       	if u :
            	auth_login(request, u)
	    	messages.add_message(request, messages.SUCCESS, 'شما با موفقیت وارد سیستم شده اید.')
		return HttpResponseRedirect(reverse('RealEstate_admin:home'))
       	else:
           	messages.add_message(request, messages.ERROR, 'نام کاربری یا رمز عبور اشتباه است.')
		return render_to_response('Luna_admin/login.html',{'username':username}, context_instance=RequestContext(request))
    else:
    	return render_to_response('Luna_admin/login.html',RequestContext(request))# context_instance=RequestContext(request))

def signout(request):
	logout(request)
	url = reverse('home',args=(),kwargs=())
        return HttpResponseRedirect(url) 

def change_password(request):
    if request.POST:
        old_pass = request.POST.get('old_pass')
        new_pass = request.POST.get('new_pass')
        repeat_new_pass = request.POST.get('repeat_new_pass')
        user = authenticate(username=request.user.username, password=old_pass)        
        if user is not None: 
		if not new_pass or not repeat_new_pass :
			messages.add_message(request, messages.ERROR, 'گذرواژه جدید و تکرار آن را وارد کنید.')     
		elif new_pass != repeat_new_pass:
			messages.add_message(request, messages.ERROR, 'گذرواژه جدید با تکرار آن یکسان نیست.')
		else:
                    user.set_password(new_pass)
	            user.save()
		    messages.add_message(request, messages.SUCCESS, 'تغییر گذرواژه با موفقیت انجام شد.')
        else:
	    messages.add_message(request, messages.ERROR, 'گذرواژه اشتباه است.')   
	return render_to_response('Luna_admin/change_password.html',{'username':request.user.username}, 				context_instance=RequestContext(request))   
            
    return render_to_response('Luna_admin/change_password.html', context_instance=RequestContext(request))	
    
def send_sms(request, cell_no=None, draft_id=None):
	if request.method == 'POST':
		form=SendSmsForm(request.POST)
		if form.is_valid():
			_server = None
			remaining_credit = None
			text = form.cleaned_data['text']
			sender_no = form.cleaned_data['sender_no']
			_sender_no_parts = sender_no.split('-')
			try:
				user = request.user
				_server = WSDL.Proxy(settings.SMS_WEB_SERVICE_URL)
				for cell_no in _sender_no_parts:
					 								
					_result = _server.SendSMS(settings.SMS_USERNAME, 							settings.SMS_PASSWORD,cell_no,text,settings.SMS_SENDER_NO)
					if(len(_result) >= 8):
						messages.add_message(request, messages.SUCCESS, u"پیامک به شماره %s ارسال شد." % cell_no) 
						sms_log = SmsLog(sender_no=cell_no,text=text, user=user)
						sms_log.save()

						
					elif _result == 12 or _result == -3:
						messages.add_message(request, messages.ERROR, 'میزان اعتبار شما به اتمام رسیده است.لطفا آن را تمدید کنید.')  
					elif _result == 20:
						messages.add_message(request, messages.ERROR, 'متن پیامک طولانی است.')  
					elif _result == 23:
						messages.add_message(request, messages.ERROR, 'تعداد دریافت کنندگان بیش از حد مجاز است.')
					else: 						
						messages.add_message(request, messages.ERROR, 'خطا در ارسال پیامک.')				
			except:
				messages.add_message(request, messages.ERROR, 'خطا در ارسال پیامک. لطفا مجددا ارسال کنید.') 
			#remaining_credit = get_remaining_sms_credit(_server)
		url = reverse('Luna_admin:sendSms',args=(),kwargs=())
        	return HttpResponseRedirect(url)
				
		#return HttpResponseRedirect("../admin/sendsms")
						
			
	else:
		form = SendSmsForm()
		default_text = ''
		if draft_id:
			if draft_id == '1':
				default_text = "فایل شما در سیستم ثبت شد."
			elif draft_id == '2' :
				default_text = "قرارداد شما در حال اتمام است."

		remaining_credit = get_remaining_sms_credit()

		sms = {'cell_no' : cell_no, 'default_text' : default_text}
		return render_to_response(
			'Luna_admin/send_sms.html',
		        {'sms':sms, 'form':form, 'remaining_credit': remaining_credit},
		        context_instance=RequestContext(request)
			)

def user_list(request):
	if not request.user.is_superuser :
		raise PermissionDenied
	users = User.objects.filter(is_superuser=False).order_by("-date_joined")
	paginator = Paginator(users, 25)
	page = request.GET.get('page')
	try:
		users_page=paginator.page(page)
		return render(request,'Luna_admin/user_list.html',{'users':users_page})
	except PageNotAnInteger:
		users_page=paginator.page(1)
		return render(request,'Luna_admin/user_list.html',{'users':users_page})
	except EmptyPage:
		return render(request,'Luna_admin/user_list.html')

def change_user_active_status(request, user_id):
	if not request.user.is_superuser :
		raise PermissionDenied
	user = get_object_or_404(User, id=user_id)	
	user.is_active = not user.is_active
	user.save()
	if user.is_active:
		messages.add_message(request, messages.SUCCESS, 'کاربر فعال شد.')
	else:
		messages.add_message(request, messages.SUCCESS, 'کاربر غیر فعال شد.')
	url = reverse('Luna_admin:user_list',args=(),kwargs=())
        return HttpResponseRedirect(url)

def add_edit_user(request, user_id) :
	if not request.user.is_superuser :
		raise PermissionDenied
	if request.method == 'POST':
		form = UserRegistrationForm(request.POST)
		if form.is_valid():
			#u_id = request.POST.get('use_id')
			user = ''
			password = form.cleaned_data['password']
			password_repeat = form.cleaned_data['password_repeat']
			if password != password_repeat:
		        	messages.add_message(request, messages.ERROR, 'گذرواژه با تکرار آن یکسان نیست.')
			else:
				
				'''if u_id != 'None':
					user = get_object_or_404(User,id=int(u_id))
					user.first_name = form.cleaned_data['first_name']
					user.last_name = form.cleaned_data['last_name']
					user.username = form.cleaned_data['username']
					user.password = form.cleaned_data['password']
					user.is_superuser = False
					user.is_staff = True'''
			
				#else:
				user = User(
					first_name = form.cleaned_data['first_name'],
					last_name = form.cleaned_data['last_name'],
					username = form.cleaned_data['username'],
					is_superuser = False,
					is_staff = True
						)
			
				try:	
					user.set_password(form.cleaned_data['password'])				
					user.save()
					messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
				except:
					 messages.add_message(request, messages.ERROR, 'نام کاربری تکراری است. لطفا آن را تغییر دهید.')   	
			#if u_id != 'None':
			#	return HttpResponseRedirect(
			#		'../add_edit_user/{}/'.format(u_id),{'user':user})
			#else:
			return render_to_response(
				'Luna_admin/add_edit_user.html',
		        	context_instance=RequestContext(request))
		#return render_to_response(
					#'Luna_admin/add_edit_user.html',{'form': form},
		        		#context_instance=RequestContext(request))
	else:
		 '''if user_id :
		 	user = get_object_or_404(User,id=user_id)
			return render_to_response(
        		'Luna_admin/add_edit_user.html',{'user':user},
                	context_instance=RequestContext(request))
		 else:'''
		 return render_to_response(
			 'Luna_admin/add_edit_user.html',
			 context_instance=RequestContext(request))
