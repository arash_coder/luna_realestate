#! /usr/bin/env python
# -*- coding: utf8 -*-

"""

@author: AR.Jozaghi
@organization: LunaSoft
@copyright: Copyright © 2014 LUNA
@license: 
@contact:
"""

from django import template
from django.db import models
import khayyam


register=template.Library()

'''@register.filter(name='format_url')
def format_url(url, page_number):
	if url.find('page') == -1:
		sdsd
	else:
		url.replace'''

@register.filter(name='is_special')
def is_special(file):
    deal = file.deal_set.filter(is_special=True)
    if deal:
        return True
    return False

@register.simple_tag(takes_context=True)
def url_add_query(context, **kwargs):
	request = context.get('request')

	get = request.GET.copy()
	get.update(kwargs)

	path = '%s?' % request.path
	for query, val in get.items():
		path += '%s=%s&' % (query, val)

	return path[:-1]

@register.filter(name='format_deal_type')
def format_deal_type(active_deal):
	 for key,val in active_deal.DEAL_TYPE:
			if active_deal.deal_type == key:
				return val

@register.filter(name='format_usage_type')
def format_usage_type(property_detail):
	 for key,val in property_detail.USAGE_TYPE:
			if property_detail.usage_type  == key:
				if property_detail.usage_type == "other":
					return property_detail.other_usage_type_name
				else:
					return val

@register.filter(name='format_property_type')
def format_property_type(property_detail):	
	 for key,val in property_detail.PROPERTY_TYPE:
			if property_detail.property_type  == key:
				if property_detail.property_type == "other":
					return property_detail.other_property_type_name
				else:
					return val

@register.filter(name='format_request_property_type')
def format_request_property_type(property_request):

	'''option_div = ''
	p = models.get_model('RealEstate_admin', 'PropertyRequest')
	for key,val in p._meta.get_field('PROPERTY_TYPE'):
			if  property_request.property_type == key :
				option_div += '<option value="{{key}}" selected >{{val}}</option>'
			else:
				option_div += '<option value="{{key}}">{{val}}</option>'

	return option_div  '''
	option_div = ''
	if property_request:
		for key,val in property_request.PROPERTY_TYPE:
			if  property_request.property_type == key :
				option_div += "<option value='"+key+"' selected >"+val+"</option>"
			else:
				option_div += "<option value='"+key+"'>"+val+"</option>"

		return option_div
	else:
		option_div += '<option value="apartment" selected >آپارتمان</option>'
		option_div += '<option value="house">خانه</option>'
		option_div += '<option value="suite">سوئیت</option>'
		option_div += '<option value="shop">مغازه</option>'
		option_div += '<option value="other">سایر</option>'
		return option_div


@register.filter(name='format_request_deal_type')
def format_request_deal_type(property_request):

	option_div = ''
	if property_request:
		for key,val in property_request.DEAL_TYPE:
			if  property_request.deal_type == key :
				option_div += "<option value='"+key+"' selected >"+val+"</option>"
			else:
				option_div += "<option value='"+key+"'>"+val+"</option>"

		return option_div
	else:
		option_div += '<option value="rahn_va_ejare" selected >رهن و اجاره</option>'
		option_div += '<option value="rahn_kamel">رهن کامل</option>'
		option_div += '<option value="forush_malekiyat">فروش مالکیت</option>'
		option_div += '<option value="forush_sarghofly">فروش سرقفلی</option>'
		option_div += '<option value="moaveze">معاوضه</option>'
		option_div += '<option value="pish_forush">پیش فروش</option>'
		option_div += '<option value="mosharekat_dar_sakht">مشارکت در ساخت</option>'
		return option_div

@register.simple_tag
def format_fields(instance,check_type):
	"""
		if check_type is 0, it only formats boolean type. if it is 1 it formats other types.
	Returns formatted rows and columns.
    """
	bool_num_items_in_a_row = 4
	str_num_items_in_a_row = 3
	#bool_bootstrap_column_size = 12 / bool_num_items_in_a_row
	bool_div = ''
	str_div = ''
	bool_counter = 1
	str_counter = 1
	if instance :
		if check_type == 0:
			for field in instance._meta.fields:
				if (type(field) == models.fields.BooleanField and getattr(instance,str(field.name)) and field.name != "is_dirty"):
					if bool_counter % bool_num_items_in_a_row == 1:
						bool_div += '<div class="row">'
					bool_div += '<div class="col-xs-4 col-sm-3"><span class="glyphicon glyphicon-ok property-value"></span>' + \
						   instance._meta.get_field(field.name).verbose_name +  '</div>'
					if bool_counter % bool_num_items_in_a_row == 0:
						bool_div += '</div>'
					bool_counter += 1

		if check_type == 1:
			for field in instance._meta.fields:
				if(type(field) == models.fields.CharField and
							   getattr(instance,field.name) != None and len(getattr(instance,field.name)) > 0):
					if str_counter % str_num_items_in_a_row == 1:
							str_div += '<div class="row">'
					str_div += '<div class="col-xs-3 col-sm-2">' + instance._meta.get_field(field.name).verbose_name + \
						   '</div>'
					if field.name == "tozihat":
						str_div += '<div class="col-xs-12 col-sm-12 property-value">' + \
						   getattr(instance,field.name).encode('utf-8') + \
						   '</div>'
					else:
						str_div += '<div class="col-xs-3 col-sm-2 property-value">' + \
						   getattr(instance,field.name).encode('utf-8') + \
						   '</div>'

					if str_counter % str_num_items_in_a_row == 0:
						str_div += '</div>'
					str_counter += 1


	if check_type == 0:
		return bool_div
	else:
		return str_div

#return persian date in YYYY/MM/DD format
@register.filter(name='persian_date')
def persian_date(value):
	try:
		return khayyam.JalaliDatetime.from_datetime(value).strftime("%Y/%m/%d")
	except:
		return ''


#return money format
@register.filter(name='money_format')
def money_format(value):
	if value != None:
		milyard=value/1000000000
		num=value%1000000000
		milyon=num/1000000
		num=num%1000000
		hezar=num/1000
		rem=num%1000
		retstr=''
		has_prev=False
		if rem!=0:
			retstr=str(rem)
			has_prev=True
		if hezar!=0:
			retstr=add_and(retstr,has_prev)
			has_prev=True
			retstr = str(hezar)+' هزار '+retstr
		if milyon!=0 :
			retstr=add_and(retstr,has_prev)
			has_prev=True
			retstr = str(milyon)+' میلیون '+retstr
		if milyard!=0:
			retstr=add_and(retstr,has_prev)
			has_prev=True
			retstr = str(milyard)+' میلیارد '+retstr
		if has_prev:
			retstr=retstr+' تومان '
		else:
			retstr='0'
		return retstr
	return ''

def add_and(retstr,has_prev):
	if has_prev:
		retstr=' و '+retstr
	return retstr


