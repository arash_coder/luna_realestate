from django.db import models
from django.contrib.auth.models import User

# Sms Model
class SmsLog(models.Model):
	sender_no = models.CharField(max_length=11)
	text = models.CharField(max_length=1000)
	send_date = models.DateField(auto_now_add=True)
	user = models.ForeignKey(User)



