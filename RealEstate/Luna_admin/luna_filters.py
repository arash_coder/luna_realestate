from django import template
import khayyam

register=template.Library()

#return persian date in YYYY/MM/DD-HH:MM format
@register.filter(name='persian_date')
def persian_date(value):
	return khayyam.JalaliDatetime.from_datetime(value).strftime("%Y/%m/%d-%H:%M")


