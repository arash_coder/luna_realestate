from django.conf.urls import patterns, url
from Luna_admin import views
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
        url(r'^login/$', views.login, name='login'),
	 url(r'^signout/$', views.signout, name='signout'),
        url(r'^change_password/$', login_required(views.change_password), name='change_password'),
	url(r'^sendsms/$', login_required(views.send_sms), name='sendSms'),
	url(r'^sendsms/(?P<cell_no>.+)/(?P<draft_id>\d+)$', login_required(views.send_sms), name='sendSms'),
	url(r'^user_list/$', login_required(views.user_list), name='user_list'),
	url(r'^change_user_active_status/(?P<user_id>\d+)/$', login_required(views.change_user_active_status), 				name='change_user_active_status'),
	url(r'^add_edit_user/((?P<user_id>\d+)/)?$',login_required(views.add_edit_user),name='add_edit_user'),
)
