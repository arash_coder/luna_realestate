$(function () {
   
    $("#remainingChars").html("تعداد کاراکترهای باقیمانده: " + (70 - ($("#text")[0]).value.length));
    
    $('#text').keyup(function(){

        if(this.value.length > 70) {
          
        }
        
        var hasUnicodeChars = false;
        for (var i = 0, n = this.value.length; i < n; i++) {
            if (this.value.charCodeAt(i) > 255) {
                hasUnicodeChars = true;
                break;
            }
        }
        var totalChars = hasUnicodeChars ? 70 : 140;
        var numberOfSms = Math.floor(this.value.length / totalChars);
        $("#remainingChars").html("تعداد پیامک: " + numberOfSms);
        $("#remainingChars").append("<br />");
        $("#remainingChars").append("تعداد کاراکترهای باقیمانده: " +(totalChars - (this.value.length % totalChars)));
    });

    var validator = new FormValidator(
       'sendSmsForm',
       [
           {
               name: 'text',
               display: 'متن پیامک',
               rules: 'required'
           },
           {
               name: 'sender_no',
               display: 'شماره های همراه',
               rules: 'required|!callback_check_smsNumbers'              
           }
       ],
         function (errors, event) {
             if (errors.length > 0) {
                 event.preventDefault();
                 var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
                 for (var i = 0; i < errors.length; i++) {
                     result += '<li>' + errors[i].message + '</li>';
                 }
                 result += '</ul></div>';
                 $('#sendSmsFormErrors').html(result);

             }
         }
    );
    
    validator.registerCallback('check_smsNumbers', function (smsNumbers) {
        var cellRegex = /^\d{11}(-\d{11})*$/;
        return (smsNumbers.match(cellRegex) !== null);
    })
.setMessage('check_smsNumbers', 'لطفاً شماره ها را صحیح وارد کنید.');
});
