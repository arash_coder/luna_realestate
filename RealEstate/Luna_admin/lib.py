#! /usr/bin/env python
# -*- coding: utf8 -*-

import khayyam
from RealEstate import settings
import json
import requests
from json import JSONEncoder
import uuid
import hashlib

# datestr : YYYY/MM/DD exp 1392/12/4
def get_date_from_persion_date_str(datestr):
	try:
		return khayyam.JalaliDatetime(int(datestr.split('/')[0]),int(datestr.split('/')[1]),int(datestr.split('/')[2]),0,0,0,0).to_datetime()
	except:
		return None

def sync_with_server(deal, request_type):

	if(request_type == 1):#add to server
		#jsonString = JSONEncoder().encode({
		   jsonString={ "is_active" : True,
			"bongah" : settings.CODE_BONGAH,
			"file_id" : deal.deal_file_id,
			"house_code" : deal.deal_file.file_code,
			"house_kind" : convert_property_detail(deal.deal_file.property_detail,deal.deal_file.property_detail.property_type),
            "deal_type" : convert_deal_type(deal, deal.deal_type),
			"area" : deal.deal_file.property_detail.area,
			"masahat" : deal.deal_file.property_detail.masahat_kol,
			"total_price" : deal.total_price,
			"rahne_kamel_price" : deal.rahne_kamel_price,
			"rahn_price" : deal.rahn_price, 
			"ejare_price" : deal.ejare_price
           }
	else:#delete from server
		jsonString ={  "is_active" : False,
			"bongah" : settings.CODE_BONGAH,
			"file_id" : deal.deal_file_id
				}
	try:
	    requests.post(settings.SERVER_ENDPOINT_URL, data=jsonString)
	except:
		deal.is_synced = False
		deal.save()

def convert_property_detail(property_detail,property_type):
     for key,val in property_detail.PROPERTY_TYPE:
         	if property_type  == key:
			    if property_detail.property_type == "other":
				    return property_detail.other_property_type_name
			    else:
				    return val

def convert_deal_type(deal, deal_type):
	 for key,val in deal.DEAL_TYPE:
         	if deal_type == key:
			    return val

def create_secret_key():
	salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + settings.SECRET_KEY.encode()).hexdigest()
	
	
def get_remaining_sms_credit(server=None):
	remaining_credit = None
	try:
		if not server:
			server = WSDL.Proxy(settings.SMS_WEB_SERVICE_URL)
		remaining_credit = server.CREDIT_LINESMS(settings.SMS_USERNAME,settings.SMS_PASSWORD,settings.SMS_SENDER_NO)
	except:
		pass
	if remaining_credit == -1 or remaining_credit == -2 :
			remaining_credit = None
	return remaining_credit

