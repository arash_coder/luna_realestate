$(function () {

 var validator = new FormValidator(
      'contactUsForm',
      [
	 {
              name: 'phone_no',
              display: 'شماره تماس',
              rules: 'numeric'
         },        
	 {
              name: 'main_text',
              display: 'متن',
              rules: 'required'
          }  
      ],
      function (errors, event) {
          if (errors.length > 0) {
              event.preventDefault();
              var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
              for (i = 0; i < errors.length; i++) {
                  result += '<li>' + errors[i].message + '</li>';
              }
              result += '</ul></div>';
              $('#contactUsFormErrors').html(result);

          }
      }
  );
   
});


