#! /usr/bin/env python
# -*- coding: utf8 -*-
from django.db import connection
from RealEstate import settings
import os
from django.http import HttpResponse
import datetime
class SpaceCalc:
	def get_db_size(self):
		cursor=connection.cursor()
		cursor.execute('''SELECT pg_size_pretty(pg_database_size(current_database())) AS dbsize''')
		row = cursor.fetchone()
		return int(row[0].split(' ')[0])*1024
	
	def get_dir_size(self):
		total_size = 0
		for dirpath, dirnames, filenames in os.walk(settings.BASE_DIR):
			for f in filenames:
				fp = os.path.join(dirpath, f)
				total_size += os.path.getsize(fp)
		return total_size



class Restrictor:
	def process_request(self,request):
		if not "space_check_time" in request.session:
			request.session["end_site_size"]=False
			request.session["space_check_time"]=str(datetime.datetime.now())
		if not "end_site_size" in request.session:
			request.session["end_site_size"]=False
		session_datetime=datetime.datetime.strptime(str( request.session["space_check_time"]),'%Y-%m-%d %H:%M:%S.%f')
		if datetime.datetime.now() >session_datetime+datetime.timedelta(seconds=1000) or request.session["end_site_size"] :
			request.session["space_check_time"]=str(datetime.datetime.now())
			calc=SpaceCalc()
			total_size= calc.get_db_size()+ calc.get_dir_size() 
			max_size_in_byte=settings.MAX_SITE_SIZE_IN_MB*1024*1024
			if total_size>max_size_in_byte:
				request.session["end_site_size"]=True
				return HttpResponse("پایان فضای مجاز با پشتیبانی تماس بگیرید")
			else:
				request.session["end_site_size"]=False

		return None
