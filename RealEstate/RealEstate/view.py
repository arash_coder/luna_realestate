#! /usr/bin/env python
# -*- coding: utf8 -*-


from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from RealEstate_admin.models import File, Deal, ContactUs
from RealEstate_admin.forms import ContactUsForm
from RealEstate_admin.models import File, Deal,Profile
from RealEstate.models import UserStatistics
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def get_statictics():
    try:
        return UserStatistics.objects.all()[0]
    except:
        return None


class Meta:
    def __init__(self, request):
        self.ip = request.META.get('REMOTE_ADDR', None)


def home(request):
    file_ids = Deal.objects.filter(is_active=True).values_list('deal_file_id', flat=True)
    files = File.objects.filter(pk__in=file_ids).order_by('-creation_date')[:3]
    number_of_file = File.objects.all().count()
    number_of_active_deal = Deal.objects.filter(is_active=True).count()
    context_files = []
    for qfile in files:
        context_files.append((qfile, qfile.deal_set.get(is_active=True)))
    special_deals = Deal.objects.filter(is_active=True, is_special=True)

    return render(request, 'home.html',
                  {'files': context_files,
                   'current_page': 'home',
                   'stat': get_statictics(),
                   'meta': Meta(request),
                   'number_of_file': number_of_file,
                   'number_of_active_deal': number_of_active_deal,
                   'special_deals': special_deals,
                   'profile': get_profile(),
                   },
                  )

def is_int(val):
    try:
        int(val)
        return True
    except:
        return False

def search(request):
    try:
        deal = request.GET['deal']
    except:
        deal = 'no_filter'
    try:
        property_type = request.GET['property']
    except:
        property_type = 'no_filter'
    try:
        meter = request.GET['meter']
    except:
        meter = 'no_filter'
    try:
        no_bed = request.GET['no_bed']
    except:
        no_bed = ''
    #filter deal type
    deal_query = Deal.objects
    deal_type_set = ['rahn_va_ejare', 'rahn_kamel', 'forush_malekiyat', 'forush_sarghofly', 'moaveze', 'pish_forush',
                     'mosharekat_dar_sakht']
    if deal in deal_type_set:
        deal_query = deal_query.filter(deal_type=deal)
    else:
        deal = 'no_filter'
    file_ids = deal_query.filter(is_active=True).values_list('deal_file_id', flat=True)

    files = File.objects.filter(pk__in=file_ids).order_by('-creation_date')
    #filter meter
    meter_set = ['250', '500', '1000', 'other']
    if meter in meter_set:
        if meter == 'other':
            files = files.filter(property_detail__masahat_kol__gte=1000)
        else:
            int_meter = int(meter)
            min_meter = 0
            max_meter = 0
            if int_meter == 250:
                max_meter = 250
            if int_meter == 500:
                min_meter = 250
                max_meter = 500
            if int_meter == 1000:
                min_meter = 500
                max_meter = 1000
            files = files.filter(property_detail__masahat_kol__lte=max_meter)
            files = files.filter(property_detail__masahat_kol__gte=min_meter)
    else:
        meter = 'no_filter'
    #filter property type
    property_type_set = ['apartment', 'house', 'suite', 'shop', 'other']
    if property_type in property_type_set:
        files = files.filter(property_detail__property_type=property_type)
    else:
        property_type = 'no_filter'

    if no_bed:
        if is_int(no_bed):
            files = files.filter(property_detail__no_bedrooms=int(no_bed))
        else:
            #messages.add_message(request, messages.ERROR, 'تعداد خواب معتبر نیست.')
            return render(request, 'search.html',
                      {'deal': deal, 'property': property_type, 'meter': meter, 'no_bed': no_bed,
                       'current_page': 'search', 'stat': get_statictics(), 'meta': Meta(request),
                       'profile': get_profile(),})


    context_files = []
    for qfile in files:
        context_files.append((qfile, qfile.deal_set.get(is_active=True)))

    #pageing
    paginator = Paginator(context_files, 12)
    page = request.GET.get('page')
    try:
        files_page = paginator.page(page)
        return render(request, 'search.html',
                      {'files': files_page, 'deal': deal, 'property': property_type, 'meter': meter,
                       'no_bed': no_bed, 'current_page': 'search', 'stat': get_statictics(), 'meta': Meta(request),
                       'profile': get_profile(),})
    except PageNotAnInteger:
        files_page = paginator.page(1)
        return render(request, 'search.html',
                      {'files': files_page, 'deal': deal, 'property': property_type, 'meter': meter,
                       'no_bed': no_bed, 'current_page': 'search', 'stat': get_statictics(), 'meta': Meta(request),
                       'profile': get_profile(),})
    except EmptyPage:
        return render(request, 'search.html',
                      {'files': files_page, 'deal': deal, 'property': property_type, 'meter': meter,
                       'no_bed': no_bed, 'current_page': 'search', 'stat': get_statictics(), 'meta': Meta(request),
                       'profile': get_profile(),})


def property_detail(request, file_id):
    property_file = get_object_or_404(File, pk=file_id)
    active_deal = property_file.deal_set.filter(is_active=True)[0]
    #File.objects.select_related('property_option').select_related('property_detail'),
    return render(request, 'detail.html',
                  {'file': property_file, 'active_deal': active_deal, 'stat': get_statictics(), 'meta': Meta(request),
                   'profile': get_profile(),})


def contact_us(request):
    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            contact_us = ContactUs(
                phone_no=form.cleaned_data['phone_no'],
                name=form.cleaned_data['name'],
                title=form.cleaned_data['title'],
                main_text=form.cleaned_data['main_text']
            )
            contact_us.save()
            messages.add_message(request, messages.SUCCESS, 'درخواست مشاوره ارسال شد.')
            return HttpResponseRedirect(reverse('contact_us'))
    else:
        return render(request, 'contact_us.html',
                      {'current_page': 'contact_us', 'stat': get_statictics(), 'meta': Meta(request),
                       'profile': get_profile(),})


def about_us(request):
    return render(request, 'about_us.html',
                  {'current_page': 'about_us', 'stat': get_statictics(), 'meta': Meta(request),
                   'profile': get_profile()})


def get_profile():
    profile = None
    profiles = Profile.objects.all()
    if profiles:
        profile = profiles[0]
    return profile