from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from RealEstate import view

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',

	url(r'^admin/', include('Luna_admin.urls',namespace='Luna_admin')),
	url(r'^realestate/admin/',include('RealEstate_admin.urls',namespace='RealEstate_admin')),
	url(r'^$',view.home,name='home'),
	url(r'^search$',view.search,name='search'),
	url(r'^detail/(?P<file_id>\d+)/$',view.property_detail,name='detail'),
	url(r'^contact_us$',view.contact_us,name='contact_us'),
	url(r'^about_us$',view.about_us,name='about_us'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += staticfiles_urlpatterns()
