from RealEstate.models import UserStatistics
import datetime
class VisitCountMid:
	def count(self):
		try:
			stat= UserStatistics.objects.all()[0]
		except:
			stat= None
		if not stat:
			us=UserStatistics()
			us.total_count=1
			us.daily_count=1
			us.today=datetime.datetime.now()
			us.save()
		else:
			stat.total_count+=1
			if stat.today.date()==datetime.datetime.today().date():
				stat.daily_count+=1
				stat.save()
			else:
				stat.today=datetime.datetime.now()
				stat.daily_count=1
				stat.save()

	def process_request(self,request):
		if 'user_was_count' not in request.session:
			request.session['user_was_count']=True
			self.count()
		return None


