"""
Django settings for RealEstate project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!_&q6g3kr6l_^^mo-w8==bt9m29p(xt=m5fymqlkm-44@111th'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = [
    'lunasoft.ir',
'www.lunasoft.ir',
]


# Application definition

INSTALLED_APPS = (
    
    #'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'Luna_admin',
    'RealEstate_admin',
    'RealEstate',
  #  'debug_toolbar',
)
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'RealEstate.VisitCountMid.VisitCountMid',
    'RealEstate.SpaceRestrictorMid.Restrictor',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',    
    "django.contrib.messages.context_processors.messages",
)

ROOT_URLCONF = 'RealEstate.urls'

WSGI_APPLICATION = 'RealEstate.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':'django.db.backends.postgresql_psycopg2',
        'NAME': 'realestate',
        'USER': 'postgres',
        'PASSWORD': '1234qsc',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_ROOT='/var/www/realestate/RealEstate/static'
STATIC_URL = '/static/'
#must set STATIC_ROOT in production.
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'

LOGIN_URL = 'Luna_admin:login'
SMS_WEB_SERVICE_URL = 'http://webservice.smscall.ir/index.php?wsdl'
SMS_USERNAME = '21606021'
SMS_PASSWORD = '2305977'
SMS_SENDER_NO = '30009900032523'
MAX_SITE_SIZE_IN_MB=300
SERVER_ENDPOINT_URL='http://shishdong.ir/house'
CODE_BONGAH=10

