from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from RealEstate_admin.models import File, Deal, PropertyRequest, PropertyDetail, PropertyOption


class UserStatistics(models.Model):
    total_count = models.IntegerField()
    daily_count = models.IntegerField()
    today = models.DateTimeField(auto_now_add=True)


@receiver(pre_save)
def set_is_dirty_when_updating(sender, instance, **kwargs):
    '''
    when updating File, Deal, PropertyDetail, PropertyOption, and PropertyRequest, set isdirty=true
    '''
    if sender in [File, Deal, PropertyDetail, PropertyOption, PropertyRequest]:
        #if it is update(not create).
        if instance.id:
            instance.is_dirty = True
