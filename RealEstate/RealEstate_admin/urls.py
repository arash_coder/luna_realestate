from django.conf.urls import patterns, url
from RealEstate_admin import files, home, end_deals, special_files
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
                       #admin home page
                       url(r'^$', login_required(home.index), name='home'),
                       # show ending deals
                       url(r'^enddeals/$', login_required(end_deals.get_deals), name="endDeals"),
                       #add new file
                       url(r'^files$', login_required(files.index), name='fileIndex'),
                       url(r'^addnewfile$', login_required(files.add_new_file), name='addNewFile'),
                       #edit file
                       url(r'^editfile/(?P<file_id>\d+)/deal', login_required(files.edit_file_deal),
                           name='editFileDeal'),
                       url(r'^editfile/editdeal$', login_required(files.edit_deal), name='editDeal'),
                       url(r'^editfile/(?P<file_id>\d+)/detail', login_required(files.edit_file_detail),
                           name='editFileDetail'),
                       url(r'^enddeal/$', login_required(files.end_deal), name='endDeal'),
                       url(r'^addnewDeal/$', login_required(files.add_new_deal), name='addNewDeal'),
                       url(r'^editfile/editownerdetail$', login_required(files.edit_detail_owner),
                           name='editOwnerDetail'),
                       url(r'^editfile/editpropertydetail$', login_required(files.edit_detail_property_detail),
                           name='editPropertyDetail'),
                       url(r'^editfile/editotherpropertydetail$', login_required(files.edit_detail_other_detail),
                           name='editOtherPropertyDetail'),
                       url(r'^editfile/(?P<file_id>\d+)/options', login_required(files.edit_file_options),
                           name='editFileOptions'),
                       url(r'^editfile/(?P<file_id>\d+)/editoptions$', login_required(files.edit_options),
                           name='editOptions'),
                       url(r'^editfile/(?P<file_id>\d+)/images', login_required(files.edit_file_images),
                           name='editFileImages'),
                       url(r'^delete_image/(?P<file_id>\d+)/(?P<image_id>\d+)/$', login_required(files.delete_image),
                           name='delete_image'),
                       url(r'^editfile/(?P<file_id>\d+)/options', login_required(files.edit_file_options),
                           name='editFileOptions'),
                       url(r'^matched_files_requests/$', login_required(files.matched_files_requests),
                           name='matchedFilesRequests'),
                       #property request
                       url(r'^property_requests$', login_required(files.property_request_index),
                           name='propertyRequests'),
                       url(r'^add_edit_request/((?P<property_request_id>\d+)/)?$',
                           login_required(files.add_edit_property_request), name='addEditPropertyRequest'),
                       url(r'^delete_property_request/(?P<property_request_id>\d+)/$',
                           login_required(files.delete_property_request), name='deletePropertyRequest'),
                       url(r'^contact_us$', login_required(files.list_contact_us), name='contactUs'),
                       url(r'^delete_contact/(?P<contact_id>\d+)/$', login_required(files.delete_contact),
                           name='deleteContact'),
                       #special deals
                       url(r'^special_deals/$', login_required(special_files.special_files), name='special'),
                       url(r'^delete_special/(?P<file_id>\d+)/$', login_required(special_files.delete),
                           name='delete_special'),

                       #profile
                       url(r'^profile/$', login_required(files.add_edit_profile),
                           name='add_edit_profile'),
                       )
