#! /usr/bin/env python
# -*- coding: utf8 -*-

from django.shortcuts import render
from RealEstate_admin.models import Deal, File
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from RealEstate_admin.forms import AddSpecialDealForm


def special_files(request):
    if request.method == 'POST':
        form = AddSpecialDealForm(request.POST)
        if form.is_valid():
            try:
                file_code = form.cleaned_data['file_code']
                code = file_code[:-1]
                code_type = file_code[-1]
                efile = File.objects.get(code=int(code), code_type=code_type.lower())
            except (File.DoesNotExist, ValueError) as e:
                messages.add_message(request, messages.ERROR, 'کد فایل معتبر نیست')
                return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
 
            try:
                deal = efile.deal_set.get(is_active=True)
                deal.is_special = True
                deal.save()
                messages.add_message(request, messages.SUCCESS, 'فایل به فایلهای ویژه اضافه شد.')
                return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
            except:
                return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
        else:
            messages.add_message(request, messages.ERROR, 'کد فایل را وارد کنید')
            return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
    else:
        special_active_deal = Deal.objects.filter(is_active=True, is_special=True)
        return render(
            request,
            'RealEstate_admin/special/index.html',
            {
                'special_deals': special_active_deal,
                'special_count': special_active_deal.count()
            }
        )


#delete deal from specials
def delete(request, file_id):
    try:
        file = File.objects.get(pk=file_id)
        deal = file.deal_set.get(is_special=True)
    except Deal.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'معامله یافت نشد.')
        return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
    if deal.is_special == False:
        messages.add_message(request, messages.ERROR, 'معامله انتخاب شده فعال نیست')
        return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))

    deal.is_special = False
    deal.save()
    messages.add_message(request, messages.SUCCESS, 'فایل از فایلهای ویژه خارج شد.')
    return HttpResponseRedirect(reverse('RealEstate_admin:fileIndex'))
	
