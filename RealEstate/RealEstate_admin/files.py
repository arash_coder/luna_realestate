#! /usr/bin/env python
# -*- coding: utf8 -*-
"""

@author: AR.Jozaghi
@organization: LunaSoft
@copyright: Copyright © 2014 LUNA
@license: 
@contact:
"""
import os
from RealEstate import settings
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from RealEstate_admin.forms import AddNewFileForm, EditDealForm, EditOwnerDetailForm, PictureForm, ContactUsForm, \
    EditPropertyOptionForm, EditPropertyDetailForm, EditOtherPropertyDetailForm, PropertyRequestForm, EndDealForm, \
    AddNewDealForm, AddNewProfileForm
from RealEstate_admin.models import File, PropertyDetail, Deal, Picture, PropertyOption, PropertyRequest, ContactUs, Profile
from django.db.models import Max
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from SOAPpy import WSDL
from django.contrib import messages
from Luna_admin import lib


def get_new_file_code():
    max_code = File.objects.all().aggregate(Max('code'))['code__max']
    if max_code is not None:
        return max_code + 1
    return 1


def get_querystr_param(request, name):
    try:
        return request.GET[name]
    except:
        return ''


def is_int(val):
    try:
        int(val)
        return True
    except:
        return False


def index(request):
    files = File.objects.all()
    code = get_querystr_param(request, 'code')
    fullname = get_querystr_param(request, 'fullname')
    area = get_querystr_param(request, 's_area')
    no_bed = get_querystr_param(request, 'no_bed')
    property_type = get_querystr_param(request, 's_property_type')
    deal_type = get_querystr_param(request, 's_deal_type')
    search_archive = get_querystr_param(request, 'search_archive')

    if fullname != '':
        files = files.filter(owner_name__contains=fullname) | files.filter(owner_family__contains=fullname)
    if area != '':
        files = files.filter(property_detail__area__contains=area)
    if no_bed != '':
        files = files.filter(property_detail__no_bedrooms=int(no_bed))
    if property_type != '':
        files = files.filter(property_detail__property_type=property_type)
    if deal_type != '':
        file_ids = Deal.objects.all().filter(deal_type=deal_type).values_list('deal_file_id', flat=True)
        files = files.filter(pk__in=file_ids)
    if code != '':
	    fcode = code[:-1]
	    code_type = code[-1]
	    try:
			files = files.filter(code=int(fcode), code_type=code_type.lower())
	    except:
			return render(request, 'RealEstate_admin/file/index.html',
                       {'files': [], 'code': code, 'fullname': fullname, 'area': area,
                       'no_bed': no_bed, 's_property_type': property_type, 's_deal_type': deal_type,
                       'search_archive': search_archive})

    if len(request.GET.items()) > 2:
    	if search_archive == '':
	    file_ids = Deal.objects.all().filter(is_active=True).values_list('deal_file_id', flat=True)
        else:
	    file_ids = Deal.objects.all().filter(is_active=False).values_list('deal_file_id', flat=True)
	files = files.filter(pk__in=file_ids)


    paginator = Paginator(files, 25)
    page = request.GET.get('page')
    try:
        files_page = paginator.page(page)
        return render(request, 'RealEstate_admin/file/index.html',
                      {'files': files_page, 'code': code, 'fullname': fullname, 'area': area,
                       'no_bed': no_bed, 's_property_type': property_type, 's_deal_type': deal_type,
                       'search_archive': search_archive})
    except PageNotAnInteger:
        files_page = paginator.page(1)
        return render(request, 'RealEstate_admin/file/index.html',
                       {'files': files_page, 'code': code, 'fullname': fullname, 'area': area,
                       'no_bed': no_bed, 's_property_type': property_type, 's_deal_type': deal_type,
                       'search_archive': search_archive})

    except EmptyPage:
        return render(request, 'RealEstate_admin/file/index.html',
                       {'files': files_page, 'code': code, 'fullname': fullname, 'area': area,
                       'no_bed': no_bed, 's_property_type': property_type, 's_deal_type': deal_type,
                       'search_archive': search_archive})


def add_new_file(request):
    if request.method == 'POST':
        form = AddNewFileForm(request.POST)
        if form.is_valid():
            new_file_code = get_new_file_code()
            #add property detail
            property_detail = PropertyDetail(
                area=form.cleaned_data['area'],
                property_type=form.cleaned_data['property_type'],
                other_property_type_name=form.cleaned_data['other_property_type_name'],
                masahat_kol=form.cleaned_data['masahat_kol'],
                address=form.cleaned_data['address'],
            )
            property_detail.save()
            #add file
            new_file = File(
                code=new_file_code,
                owner_name=form.cleaned_data['owner_name'],
                owner_family=form.cleaned_data['owner_family'],
                owner_cell=form.cleaned_data['owner_cell'],
                property_detail=property_detail
            )
            new_file.save()
            #add active deal
            deal = Deal(
                deal_type=form.cleaned_data['deal_type'],
                total_price=form.cleaned_data['total_price'],
                rahne_kamel_price=form.cleaned_data['rahne_kamel_price'],
                rahn_price=form.cleaned_data['rahn_price'],
                ejare_price=form.cleaned_data['ejare_price'],
                deal_file=new_file,
                is_active=True,
                is_synced=form.cleaned_data['is_synced']
            )
            deal.save()
            if deal.is_synced:
                lib.sync_with_server(deal, 1)

            return HttpResponseRedirect("../admin/editfile/{0}/deal".format(new_file.pk))


#edit files
#edit deal -------------------------------------------------------------
def edit_file_deal(request, file_id):
    editable_file = get_object_or_404(File, pk=file_id)
    #get active deal
    try:
        active_deal = editable_file.deal_set.get(is_active=True)
    except Deal.DoesNotExist:
        active_deal = None
    inactive_deals = editable_file.deal_set.filter(is_active=False)
    return render(request, 'RealEstate_admin/file/edit_file_deal.html'
                  , {
            'editable_file': editable_file,
            'active_deal': active_deal,
            'inactive_deals': inactive_deals,
            'deal': 'active'
        })


def edit_deal(request):
    if request.method == 'POST':
        form = EditDealForm(request.POST)
        if form.is_valid():
            deal_id = form.cleaned_data['deal_id']
            active_deal = get_object_or_404(Deal, id=deal_id)
            active_deal.deal_type = form.cleaned_data['deal_type']
            active_deal.bazdid_type = form.cleaned_data['bazdid_type']
            active_deal.name_mostajer_ya_negahban = form.cleaned_data['name_mostajer_ya_negahban']
            active_deal.tell_mostajer_ya_negahban = form.cleaned_data['tell_mostajer_ya_negahban']
            active_deal.total_price = form.cleaned_data['total_price']
            active_deal.meter_price = form.cleaned_data['meter_price']
            active_deal.rahne_kamel_price = form.cleaned_data['rahne_kamel_price']
            active_deal.rahn_price = form.cleaned_data['rahn_price']
            active_deal.ejare_price = form.cleaned_data['ejare_price']
            active_deal.ghabel_tabdil = form.cleaned_data['ghabel_tabdil']
            active_deal.charge_price = form.cleaned_data['charge_price']
            active_deal.tarikhe_takhliye = lib.get_date_from_persion_date_str(form.cleaned_data['tarikhe_takhliye'])
            if (not active_deal.is_synced):
                active_deal.is_synced = form.cleaned_data['is_synced']
            active_deal.save()
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            if (form.cleaned_data['is_synced'] == True):
                lib.sync_with_server(active_deal, 1)

            return HttpResponseRedirect("../editfile/{0}/deal".format(form.cleaned_data['file_id']))


# end deal
def end_deal(request):
    if request.method == 'POST':
        form = EndDealForm(request.POST)
        if form.is_valid():
            deal_id = form.cleaned_data['deal_id']
            deal = get_object_or_404(Deal, id=deal_id)
            deal.is_active = False
            deal.is_special = False
            deal.deal_name = form.cleaned_data['deal_name']
            deal.deal_family = form.cleaned_data['deal_family']
            deal.deal_cell = form.cleaned_data['deal_cell']
            deal.deal_end_date = lib.get_date_from_persion_date_str(form.cleaned_data['end_deal_date'])
            deal.deal_description = form.cleaned_data['deal_description']
            deal.save()
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            if deal.is_synced:
                lib.sync_with_server(deal, 2)
            return HttpResponseRedirect("../editfile/{0}/deal".format(form.cleaned_data['file_id']))


# add new deal
def add_new_deal(request):
    if request.method == 'POST':

        form = AddNewDealForm(request.POST)
        if form.is_valid():
            file_id = form.cleaned_data['file_id']
            selected_file = get_object_or_404(File, pk=file_id)
            if selected_file.deal_set.filter(is_active=True).count() == 0:
                deal = Deal()
                deal.is_active = True
                deal.is_synced = False
                selected_file.deal_set.add(deal)
                selected_file.save()
                messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
                return HttpResponseRedirect("../editfile/{0}/deal".format(form.cleaned_data['file_id']))


#edit detail -----------------------------------------------------------
def edit_file_detail(request, file_id):
    editable_file = get_object_or_404(File, pk=file_id)
    return render(request, 'RealEstate_admin/file/edit_file_detail.html'
                  , {
            'editable_file': editable_file,
            'detail': 'active'
        })


#edit owner detail
def edit_detail_owner(request):
    if request.method == 'POST':       
        form = EditOwnerDetailForm(request.POST)
        if form.is_valid():           
            file_id = form.cleaned_data['file_id']
            editable_file = get_object_or_404(File, pk=file_id)
            editable_file.owner_name = form.cleaned_data['owner_name']
            editable_file.owner_family = form.cleaned_data['owner_family']
            editable_file.owner_cell = form.cleaned_data['owner_cell']
            editable_file.owner_tell = form.cleaned_data['owner_tell']           
            editable_file.save()           
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            return HttpResponseRedirect("../editfile/{0}/detail".format(file_id))


#edit property detail
def edit_detail_property_detail(request):
    if request.method == 'POST':
        form = EditPropertyDetailForm(request.POST)
        if form.is_valid():
            file_id = form.cleaned_data['file_id']
            property_detail_id = form.cleaned_data['property_detail_id']
            detail = get_object_or_404(PropertyDetail, id=property_detail_id)
            detail.property_type = form.cleaned_data['property_type']
            detail.other_property_type_name = form.cleaned_data['other_property_type_name']
            detail.usage_type = form.cleaned_data['usage_type']
            detail.other_usage_type_name = form.cleaned_data['other_usage_type_name']
            detail.address = form.cleaned_data['address']
            detail.area = form.cleaned_data['area']
            detail.no_bedrooms = form.cleaned_data['no_bedrooms']
            detail.no_floor = form.cleaned_data['no_floor']
            detail.floors = form.cleaned_data['floors']
            detail.tedade_vahed = form.cleaned_data['tedade_vahed']
            detail.masahat_zamin = form.cleaned_data['masahat_zamin']
            detail.masahat_kol = form.cleaned_data['masahat_kol']
            detail.zirbana = form.cleaned_data['zirbana']
            detail.ToolBar = form.cleaned_data['ToolBar']
            detail.save()
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            return HttpResponseRedirect("../editfile/{0}/detail".format(file_id))


#edit other property detail
def edit_detail_other_detail(request):
    if request.method == 'POST':
        form = EditOtherPropertyDetailForm(request.POST)
        if form.is_valid():
            file_id = form.cleaned_data['file_id']
            property_detail_id = form.cleaned_data['property_detail_id']
            detail = get_object_or_404(PropertyDetail, id=property_detail_id)
            detail.zabete_sakht = form.cleaned_data['zabete_sakht']
            detail.sene_bana = form.cleaned_data['sene_bana']
            detail.mogheiyat = form.cleaned_data['mogheiyat']
            detail.noe_sanad = form.cleaned_data['noe_sanad']
            detail.noe_saghf = form.cleaned_data['noe_saghf']
            detail.vame_banki = form.cleaned_data['vame_banki']
            detail.nama = form.cleaned_data['nama']
            detail.view_ya_manzare = form.cleaned_data['view_ya_manzare']
            detail.save()
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            return HttpResponseRedirect("../editfile/{0}/detail".format(file_id))


#edit options ----------------------------------------------------------
def edit_file_options(request, file_id):
    editable_file = get_object_or_404(File, pk=file_id)
    option = editable_file.property_option
    return render(request, 'RealEstate_admin/file/edit_file_options.html'
                  , {
            'option': option,
            'editable_file': editable_file,
            'options': 'active'
        })


def edit_options(request, file_id):
    if request.method == 'POST':
        form = EditPropertyOptionForm(request.POST)
        if form.is_valid():
            editable_file = get_object_or_404(File, pk=file_id)
            old_option = editable_file.property_option
            if old_option is None:
                option = PropertyOption(
                    sarmayesh=form.cleaned_data['sarmayesh'],
                    garmayesh=form.cleaned_data['garmayesh'],
                    kaf_saloon=form.cleaned_data['kaf_saloon'],
                    kaf_khabha=form.cleaned_data['kaf_khabha'],
                    badane_divar=form.cleaned_data['badane_divar'],
                    tozihat=form.cleaned_data['tozihat'],
                    mahvare_markazi=form.cleaned_data['mahvare_markazi'],
                    internet_wireless=form.cleaned_data['internet_wireless'],
                    jarubarghi_markazi=form.cleaned_data['jarubarghi_markazi'],
                    shooting_zobale=form.cleaned_data['shooting_zobale'],
                    salon_ejtemaat=form.cleaned_data['salon_ejtemaat'],
                    liting=form.cleaned_data['liting'],
                    estakhr=form.cleaned_data['estakhr'],
                    sona=form.cleaned_data['sona'],
                    jakuzi=form.cleaned_data['jakuzi'],
                    salon_varzesh=form.cleaned_data['salon_varzesh'],
                    roof_garden=form.cleaned_data['roof_garden'],
                    barbecu=form.cleaned_data['barbecu'],
                    elam_harigh=form.cleaned_data['elam_harigh'],
                    etfa_harigh=form.cleaned_data['etfa_harigh'],
                    ab_taht_feshar=form.cleaned_data['ab_taht_feshar'],
                    kabinet=form.cleaned_data['kabinet'],
                    gaaz_va_fer_tookar=form.cleaned_data['gaaz_va_fer_tookar'],
                    Open=form.cleaned_data['Open'],
                    iphone=form.cleaned_data['iphone'],
                    parde=form.cleaned_data['parde'],
                    komod=form.cleaned_data['komod'],
                    looster=form.cleaned_data['looster'],
                    shoomine=form.cleaned_data['shoomine'],
                    master=form.cleaned_data['master'],
                    kabin_doosh=form.cleaned_data['kabin_doosh'],
                    noorpardazi=form.cleaned_data['noorpardazi'],
                    noorgir_pasiyo=form.cleaned_data['noorgir_pasiyo'],
                    tras=form.cleaned_data['tras'],
                    anbari=form.cleaned_data['anbari'],
                    parking=form.cleaned_data['parking'],
                    asansor=form.cleaned_data['asansor'],
                    darb_automatik=form.cleaned_data['darb_automatik'],
                    vorudi_zed_serghat=form.cleaned_data['vorudi_zed_serghat'],
                    hayate_ekhtesasi=form.cleaned_data['hayate_ekhtesasi'],
                    labi=form.cleaned_data['labi']
                )
                option.save()
                editable_file.property_option = option
                editable_file.save()
            else:
                old_option.sarmayesh = form.cleaned_data['sarmayesh']
                old_option.garmayesh = form.cleaned_data['garmayesh']
                old_option.kaf_saloon = form.cleaned_data['kaf_saloon']
                old_option.kaf_khabha = form.cleaned_data['kaf_khabha']
                old_option.badane_divar = form.cleaned_data['badane_divar']
                old_option.tozihat = form.cleaned_data['tozihat']
                old_option.mahvare_markazi = form.cleaned_data['mahvare_markazi']
                old_option.internet_wireless = form.cleaned_data['internet_wireless']
                old_option.jarubarghi_markazi = form.cleaned_data['jarubarghi_markazi']
                old_option.shooting_zobale = form.cleaned_data['shooting_zobale']
                old_option.salon_ejtemaat = form.cleaned_data['salon_ejtemaat']
                old_option.liting = form.cleaned_data['liting']
                old_option.estakhr = form.cleaned_data['estakhr']
                old_option.sona = form.cleaned_data['sona']
                old_option.jakuzi = form.cleaned_data['jakuzi']
                old_option.salon_varzesh = form.cleaned_data['salon_varzesh']
                old_option.roof_garden = form.cleaned_data['roof_garden']
                old_option.barbecu = form.cleaned_data['barbecu']
                old_option.elam_harigh = form.cleaned_data['elam_harigh']
                old_option.etfa_harigh = form.cleaned_data['etfa_harigh']
                old_option.ab_taht_feshar = form.cleaned_data['ab_taht_feshar']
                old_option.kabinet = form.cleaned_data['kabinet']
                old_option.gaaz_va_fer_tookar = form.cleaned_data['gaaz_va_fer_tookar']
                old_option.Open = form.cleaned_data['Open']
                old_option.iphone = form.cleaned_data['iphone']
                old_option.parde = form.cleaned_data['parde']
                old_option.komod = form.cleaned_data['komod']
                old_option.looster = form.cleaned_data['looster']
                old_option.shoomine = form.cleaned_data['shoomine']
                old_option.master = form.cleaned_data['master']
                old_option.kabin_doosh = form.cleaned_data['kabin_doosh']
                old_option.noorpardazi = form.cleaned_data['noorpardazi']
                old_option.noorgir_pasiyo = form.cleaned_data['noorgir_pasiyo']
                old_option.tras = form.cleaned_data['tras']
                old_option.anbari = form.cleaned_data['anbari']
                old_option.parking = form.cleaned_data['parking']
                old_option.asansor = form.cleaned_data['asansor']
                old_option.darb_automatik = form.cleaned_data['darb_automatik']
                old_option.vorudi_zed_serghat = form.cleaned_data['vorudi_zed_serghat']
                old_option.hayate_ekhtesasi = form.cleaned_data['hayate_ekhtesasi']
                old_option.labi = form.cleaned_data['labi']
                old_option.save()
            messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
            return HttpResponseRedirect("../{0}/options".format(file_id))


def edit_file_images(request, file_id):
	
    if request.method == 'POST':
        try:
            form = PictureForm(request.POST, request.FILES)
            if form.is_valid():
                f = get_object_or_404(File, pk=file_id)
                num_pic = Picture.objects.filter(pic_file=file_id).count()
                if num_pic >= 5:
                    messages.add_message(request, messages.ERROR,
                                         ' برای یک فایل نمی توان بیشتر از ۵ تصویر بارگذاری کرد.')
                    return HttpResponseRedirect("../{0}/images".format(file_id))

                else:
                    newpic = Picture(pic_file=f, pic=request.FILES['pic'], caption=form.cleaned_data['caption'])
                    newpic.save()
                    messages.add_message(request, messages.SUCCESS, 'تصویر بارگذاری شد.')
                    return HttpResponseRedirect("../{0}/images".format(file_id))
        except IOError:
            messages.add_message(request, messages.ERROR, 'فرمت فایل انتخاب شده از نوع تصویر نیست.')
            return HttpResponseRedirect("../{0}/images".format(file_id))
    else:
        form = PictureForm()  # A empty, unbound form

        # Load documents for the list page
        pictures = Picture.objects.filter(pic_file=file_id)
        editable_file = get_object_or_404(File, pk=file_id)
        # Render list page with the documents and the form
        return render(request,
            'RealEstate_admin/file/edit_file_images.html',
            {'pictures': pictures, 'form': form, 'editable_file': editable_file, 'images': 'active'},
        )


def delete_image(request, file_id, image_id):
    try:
        pic = Picture.objects.get(id=image_id)
    except Picture.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'تصویر یافت نشد.')
        return HttpResponseRedirect(reverse('RealEstate_admin:editFileImages', args=(file_id,)))

    pic.delete()
    try:
        os.remove(os.path.join(settings.MEDIA_ROOT, pic.pic.name))
    except:
        pass
    messages.add_message(request, messages.SUCCESS, 'تصویر با موفقیت حذف شد.')
    return HttpResponseRedirect(reverse('RealEstate_admin:editFileImages', args=(file_id,)))


def add_edit_property_request(request, property_request_id):
    if request.method == 'POST':
        form = PropertyRequestForm(request.POST)
        if form.is_valid():
            request_id = form.cleaned_data['property_request_id']
            if request_id:
                property_request = get_object_or_404(PropertyRequest, id=request_id)
                property_request.name = form.cleaned_data['name']
                property_request.family = form.cleaned_data['family']
                property_request.phone_no = form.cleaned_data['phone_no']
                property_request.total_price = form.cleaned_data['total_price']
                property_request.rahne_kamel_price = form.cleaned_data['rahne_kamel_price']
                property_request.rahn_price = form.cleaned_data['rahn_price']
                property_request.ejare_price = form.cleaned_data['ejare_price']
                property_request.property_type = form.cleaned_data['property_type']
                property_request.other_property_type_name = form.cleaned_data['other_property_type_name']
                property_request.deal_type = form.cleaned_data['deal_type']
                property_request.area = form.cleaned_data['area']
                property_request.description = form.cleaned_data['description']
                property_request.save()
                messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
                return HttpResponseRedirect(
                    '../add_edit_request/{}/'.format(request_id), {'property_request': property_request})

            else:
                property_request = PropertyRequest(
                    name=form.cleaned_data['name'],
                    family=form.cleaned_data['family'],
                    phone_no=form.cleaned_data['phone_no'],
                    total_price=form.cleaned_data['total_price'],
                    rahne_kamel_price=form.cleaned_data['rahne_kamel_price'],
                    rahn_price=form.cleaned_data['rahn_price'],
                    ejare_price=form.cleaned_data['ejare_price'],
                    property_type=form.cleaned_data['property_type'],
                    other_property_type_name=form.cleaned_data['other_property_type_name'],
                    deal_type=form.cleaned_data['deal_type'],
                    area=form.cleaned_data['area'],
                    description=form.cleaned_data['description']
                )
                property_request.save()
                messages.add_message(request, messages.SUCCESS, 'تغییرات اعمال شد.')
                return render_to_response(
                    'RealEstate_admin/request/add_edit_request.html',
                    context_instance=RequestContext(request))
            #return HttpResponse(property_request_id)
    else:
        if property_request_id:
            property_request = get_object_or_404(PropertyRequest, id=property_request_id)
            return render_to_response(
                'RealEstate_admin/request/add_edit_request.html', {'property_request': property_request},
                context_instance=RequestContext(request))
        else:
            return render_to_response(
                'RealEstate_admin/request/add_edit_request.html',
                context_instance=RequestContext(request))


def property_request_index(request):

    property_requests = PropertyRequest.objects.all().order_by("-creation_date")
    fullname = get_querystr_param(request, 'fullname')
    area = get_querystr_param(request, 'area')
    property_type = get_querystr_param(request, 'property_type')
    deal_type = get_querystr_param(request, 'deal_type')

    if fullname != '':
        property_requests = property_requests.filter(name__contains=fullname) | \
                            property_requests.filter(family__contains=fullname)
    if area != '':
        property_requests = property_requests.filter(area__contains=area)
    if property_type != '':
        property_requests = property_requests.filter(property_type=property_type)
    if deal_type != '':
        property_requests = property_requests.filter(deal_type=deal_type)

    paginator = Paginator(property_requests, 25)
    page = request.GET.get('page')
    try:
        property_requests_page = paginator.page(page)
        return render(request, 'RealEstate_admin/request/index.html',
                      {'property_requests': property_requests_page, 'fullname': fullname,
                       'area': area, 'property_type': property_type, 'deal_type': deal_type})
    except PageNotAnInteger:
        property_requests_page = paginator.page(1)
        return render(request, 'RealEstate_admin/request/index.html',
                       {'property_requests': property_requests_page, 'fullname': fullname,
                       'area': area, 'property_type': property_type, 'deal_type': deal_type})
    except EmptyPage:
        return render(request, 'RealEstate_admin/request/index.html',
                       {'property_requests': property_requests_page, 'fullname': fullname,
                       'area': area, 'property_type': property_type, 'deal_type': deal_type})


def delete_property_request(request, property_request_id):
    property_req = get_object_or_404(PropertyRequest, id=property_request_id)
    property_req.delete()
    messages.add_message(request, messages.SUCCESS, 'حذف با موفقیت انجام شد.')
    return HttpResponseRedirect(reverse('RealEstate_admin:propertyRequests'))


def list_contact_us(request):
    contacts = ContactUs.objects.all().order_by("-creation_date")
    paginator = Paginator(contacts, 25)
    page = request.GET.get('page')
    try:
        contacts_page = paginator.page(page)
        return render(request, 'RealEstate_admin/contact_us/list.html', {'contacts': contacts_page})
    except PageNotAnInteger:
        contacts_page = paginator.page(1)
        return render(request, 'RealEstate_admin/contact_us/list.html', {'contacts': contacts_page})
    except EmptyPage:
        return render(request, 'RealEstate_admin/contact_us/list.html')

def delete_contact(request, contact_id):
    contact_us = get_object_or_404(ContactUs, id=contact_id)
    contact_us.delete()
    messages.add_message(request, messages.SUCCESS, 'حذف با موفقیت انجام شد.')
    url = reverse('RealEstate_admin:contactUs',args=(),kwargs=())
    return HttpResponseRedirect(url)

def matched_files_requests(request):
    active_deals = Deal.objects.filter(is_active=True)
    property_requests = PropertyRequest.objects.all().order_by("creation_date")


    #import pdb; pdb.set_trace()

    matched_dict = {}
    matched_deals = []
    for property_request in property_requests:
        matched_deals = []
        for active_deal in active_deals:
            if (property_request.deal_type == active_deal.deal_type) and \
                    (property_request.total_price >= active_deal.total_price) and \
                    (property_request.rahne_kamel_price >= active_deal.rahne_kamel_price) and \
                    (property_request.rahn_price >= active_deal.rahn_price) and \
                    (property_request.ejare_price >= active_deal.ejare_price) and \
                    (property_request.property_type == active_deal.deal_file.property_detail.property_type):
                matched_deals.append(active_deal)
        if matched_deals:
            matched_dict[property_request] = matched_deals
    return render(request, 'RealEstate_admin/file/matched_files_requests.html', {'matched_dict': matched_dict})


def add_edit_profile(request):
    if request.method == 'GET':
        profiles = Profile.objects.all()
        if profiles:
            return render(request, 'RealEstate_admin/profile/add_edit_profile.html', {'profile': profiles[0]})
        else:
            return render(request, 'RealEstate_admin/profile/add_edit_profile.html')
    if request.method == 'POST':
        profile_id = request.POST['profile_id']

        if profile_id:
            profile = get_object_or_404(Profile, pk=profile_id)
            form = AddNewProfileForm(request.POST, request.FILES, instance=profile)
        else:
            form = AddNewProfileForm(request.POST, request.FILES)

        if form.is_valid():
            profile = form.save()
            messages.add_message(request, messages.SUCCESS, 'اطلاعات با موفقیت ثبت شد.')
            return HttpResponseRedirect(reverse('RealEstate_admin:add_edit_profile'), {'profile': profile})



