#! /usr/bin/env python
# -*- coding: utf8 -*-
"""
@author: AR.Jozaghi
@organization: LunaSoft
@copyright: Copyright © 2014 LUNA
@license:
@contact:
"""
from django import forms
from django.forms import ModelForm
from RealEstate_admin import models


class AddNewFileForm(forms.Form):
    owner_name = forms.CharField(max_length=100, required=False)
    owner_family = forms.CharField(max_length=100)
    owner_cell = forms.CharField(max_length=11)
    area = forms.CharField(max_length=100)
    property_type = forms.CharField(max_length=20)
    other_property_type_name = forms.CharField(max_length=100, required=False)
    deal_type = forms.CharField(max_length=20)
    masahat_kol = forms.IntegerField(required=False)
    total_price = forms.IntegerField(required=False)
    rahne_kamel_price = forms.IntegerField(required=False)
    rahn_price = forms.IntegerField(required=False)
    ejare_price = forms.IntegerField(required=False)
    address = forms.CharField(max_length=200)
    is_synced = forms.BooleanField(required=False)


class EditDealForm(forms.Form):
    deal_type = forms.CharField(max_length=20)
    bazdid_type = forms.CharField(max_length=20)
    name_mostajer_ya_negahban = forms.CharField(max_length=200, required=False)
    tell_mostajer_ya_negahban = forms.CharField(max_length=20, required=False)
    tarikhe_takhliye = forms.CharField(max_length=12, required=False)
    total_price = forms.IntegerField(required=False)
    meter_price = forms.IntegerField(required=False)
    rahne_kamel_price = forms.IntegerField(required=False)
    rahn_price = forms.IntegerField(required=False)
    ejare_price = forms.IntegerField(required=False)
    ghabel_tabdil = forms.CharField(max_length=100, required=False)
    charge_price = forms.IntegerField(required=False)
    deal_id = forms.IntegerField()
    file_id = forms.IntegerField()
    is_synced = forms.BooleanField(required=False)


class EditOwnerDetailForm(forms.Form):
    owner_name = forms.CharField(max_length=100)
    owner_family = forms.CharField(max_length=100)
    owner_tell = forms.CharField(max_length=11, required=False)
    owner_cell = forms.CharField(max_length=20)
    file_id = forms.IntegerField()


class EditPropertyDetailForm(forms.Form):
    property_type = forms.CharField(max_length=20)
    other_property_type_name = forms.CharField(max_length=200, required=False)
    usage_type = forms.CharField(max_length=20)
    other_usage_type_name = forms.CharField(max_length=200, required=False)
    address = forms.CharField(max_length=200, required=False)
    area = forms.CharField(max_length=100)
    no_bedrooms = forms.IntegerField(required=False)
    no_floor = forms.CharField(max_length=50, required=False)
    floors = forms.CharField(max_length=50, required=False)
    tedade_vahed = forms.CharField(max_length=50, required=False)
    masahat_zamin = forms.IntegerField(required=False)
    masahat_kol = forms.IntegerField()
    zirbana = forms.IntegerField(required=False)
    ToolBar = forms.IntegerField(required=False)
    file_id = forms.IntegerField()
    property_detail_id = forms.IntegerField()


class EditOtherPropertyDetailForm(forms.Form):
    file_id = forms.IntegerField()
    property_detail_id = forms.IntegerField()
    zabete_sakht = forms.CharField(max_length=100, required=False)
    sene_bana = forms.IntegerField(required=False)
    mogheiyat = forms.CharField(max_length=100, required=False)
    noe_sanad = forms.CharField(max_length=100, required=False)
    noe_saghf = forms.CharField(max_length=100, required=False)
    vame_banki = forms.CharField(max_length=100, required=False)
    nama = forms.CharField(max_length=100, required=False)
    view_ya_manzare = forms.CharField(max_length=100, required=False)


class EditPropertyOptionForm(forms.Form):
    sarmayesh = forms.CharField(max_length=500, required=False)
    garmayesh = forms.CharField(max_length=500, required=False)
    kaf_saloon = forms.CharField(max_length=500, required=False)
    kaf_khabha = forms.CharField(max_length=500, required=False)
    badane_divar = forms.CharField(max_length=500, required=False)

    tozihat = forms.CharField(max_length=1000, required=False)
    mahvare_markazi = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    internet_wireless = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    jarubarghi_markazi = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    shooting_zobale = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    salon_ejtemaat = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    liting = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    estakhr = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    sona = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    jakuzi = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    salon_varzesh = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    roof_garden = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    barbecu = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    elam_harigh = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    etfa_harigh = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    ab_taht_feshar = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    kabinet = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    gaaz_va_fer_tookar = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    Open = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    iphone = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    parde = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    komod = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    looster = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    shoomine = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    master = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    kabin_doosh = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    noorpardazi = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    noorgir_pasiyo = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    tras = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    anbari = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    parking = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    asansor = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    darb_automatik = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    vorudi_zed_serghat = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    hayate_ekhtesasi = forms.BooleanField(required=False, widget=forms.CheckboxInput())
    labi = forms.BooleanField(required=False, widget=forms.CheckboxInput())


class PictureForm(forms.Form):
    pic = forms.FileField(label='انتخاب فایل :')
    caption = forms.CharField(max_length=100, required=False)

class PropertyRequestForm(forms.Form):
    property_request_id = forms.IntegerField(required=False)
    name = forms.CharField(max_length=100, required=False)
    family = forms.CharField(max_length=100, required=False)
    phone_no = forms.CharField(max_length=20, required=False)
    total_price = forms.IntegerField(required=False)
    rahne_kamel_price = forms.IntegerField(required=False)
    rahn_price = forms.IntegerField(required=False)
    ejare_price = forms.IntegerField(required=False)
    property_type = forms.CharField(max_length=20)
    other_property_type_name = forms.CharField(max_length=200, required=False)
    deal_type = forms.CharField(max_length=20)
    area = forms.CharField(max_length=100, required=False)
    description = forms.CharField(max_length=1000, required=False)

class EndDealForm(forms.Form):
    end_deal_date = forms.CharField(max_length=100, required=False)
    deal_name = forms.CharField(max_length=100, required=False)
    deal_family = forms.CharField(max_length=100, required=False)
    deal_cell = forms.CharField(max_length=100, required=False)
    deal_description = forms.CharField(max_length=300, required=False)
    file_id = forms.IntegerField()
    deal_id = forms.IntegerField()


class AddNewDealForm(forms.Form):
    file_id = forms.IntegerField()


class ContactUsForm(forms.Form):
    title = forms.CharField(max_length=100, required=False)
    name = forms.CharField(max_length=100, required=False)
    main_text = forms.CharField(max_length=1000)
    phone_no = forms.CharField(max_length=20, required=False)


class AddSpecialDealForm(forms.Form):
    file_code = forms.CharField()


class AddNewProfileForm(ModelForm):
    class Meta:
        model = models.Profile
