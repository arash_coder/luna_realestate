

    var validator = new FormValidator(
        'profileForm',
        [        
            {
                name: 'phone_no',
                display: 'شماره ثابت',
                rules: 'numeric|max_length[11]'
            },
         	  {
                name: 'mobile_no',
                display: 'شماره همراه',
                rules: 'numeric|max_length[11]'
            },
        ],
        function(errors, event) {
            if (errors.length>0) {
                event.preventDefault();
                var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
                for (var i = 0; i < errors.length; i++) {
                    result += '<li>' + errors[i].message + '</li>';
                }
                result += '</ul></div>';
                $('#profileFormErrors').html(result);

            }
        }
    );
