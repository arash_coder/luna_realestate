$(function () {
var validator = new FormValidator(
        'uploadForm',
        [
            {
                name: 'pic',
                display: 'فایل',
                rules: 'required'
            }
	],
        function(errors, event) {
            if (errors.length>0) {
                event.preventDefault();
                var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
                for (var i = 0; i < errors.length; i++) {
                    result += '<li>' + errors[i].message + '</li>';
                }
                result += '</ul></div>';
                $('#uploadFormErrors').html(result);

            }
        }
    );
});
