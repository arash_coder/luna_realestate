﻿$(document).ready(function() {
    $("#tarikhe_takhliye").mask("9999/99/99");
     $("#end_deal_date").mask("9999/99/99");
     
	var endDealValidator=new FormValidator(
		'end_deal',
		[
			{
				name:'deal_name',
				display:'نام',
				rules:'required'
			},
			{
				name:'deal_family',
				display:'نام خانوادگی',
				rules:'required'
			},
			{
				name:'deal_cell',
				display:'تلفن همراه',
				rules:'required'
			}
		],
		function(errors, event){
			if (errors.length > 0) {
			event.preventDefault();
			var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
			for (i = 0; i < errors.length; i++) {
				result += '<li>' + errors[i].message + '</li>';
			}
			result += '</ul></div>';
			$('#end_dealFormErrors').html(result);
			}
		});
     
    var validator = new FormValidator(
      'EditDeal',
      [
          {
              name: 'total_price',
              display: 'قیمت کل',
              rules: 'numeric'
          },
          {
              name: 'meter_price',
              display: 'قیمت هر متر',
              rules: 'numeric'
          },
          {
              name: 'rahne_kamel_price',
              display: 'رهن کامل',
              rules: 'numeric'
          },
           {
              name: 'rahn_price',
              display: 'رهن کامل',
              rules: 'numeric'
          },
          {
              name: 'ejare_price',
              display: 'اجاره',
              rules: 'numeric'
          },
          {
              name: 'ell_mostajer_ya_negahban',
              display: 'تلفن مستاجر/ نگهبان',
              rules: 'numeric'
          } ,
          {
              name: 'charge_price',
              display: 'مبلغ شارژ',
              rules: 'numeric'
          }
      ],
      function (errors, event) {
          if (errors.length > 0) {
              event.preventDefault();
              var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
              for (i = 0; i < errors.length; i++) {
                  result += '<li>' + errors[i].message + '</li>';
              }
              result += '</ul></div>';
              $('#EditDealFormError').html(result);

          }
      }
  );
  
  
});
