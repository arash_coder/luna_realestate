﻿/// <reference path="../validate.js" />
/// <reference path="../jquery-1.10.2.min.js" />
$(function () {

    $("#property_type").change(function () {
        var other = $("#OtherKind");
        if ($(this).val() === "other") {
            other.removeClass("hidden");
        } else {
            if (!other.hasClass("hidden")) {
                other.addClass("hidden");
            }
        }
    });



    var validator = new FormValidator(
        'addNewFileForm',
        [
            {
                name: 'owner_family',
                display: 'نام خانوادگی مالک',
                rules:'required|max_length[100]'
            },
            {
                name: 'owner_cell',
                display: 'تلفن همراه',
                rules: 'required|numeric|exact_length[11]'
            },
            {
            	name:'area',
            	display:'محدوده',
            	rules:'required|max_length[100]'
            },
            {
                name: 'masahat_kol',
                display: 'مساحت کل',
                rules: 'required|numeric'
            },
            {
                name: 'total_price',
                display: 'قیمت کل',
                rules: 'numeric'
            },
            {
                name: 'rahn_price',
                display: 'رهن',
                rules: 'numeric'
            },
            {
                name: 'ejare_price',
                display: 'اجاره',
                rules: 'numeric'
            },
            {
                name: 'Other',
                display:' ',
                rules: '!callback_check_other|max_length[100]'
            },
            {
            	name:'address',
            	display:'آدرس کامل',
            	rules:'required|max_length[200]'
            }
            
            
        ],
        function(errors, event) {
            if (errors.length>0) {
                event.preventDefault();
                var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
                for (var i = 0; i < errors.length; i++) {
                    result += '<li>' + errors[i].message + '</li>';
                }
                result += '</ul></div>';
                $('#addNewFileFormErrors').html(result);

            }
        }
    );
    validator.registerCallback('check_other', function (value) {
        if ($("#PropertyType").val()==="5")
        if ($("#Other").val().trim() == "") {
            return false;
        }

        return true;
    })
.setMessage('check_other', 'نوع ملک را در فیلد سایر وارد کنید');

});
