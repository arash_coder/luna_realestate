﻿function OtherPropertyTypeState() {
    var propertyType = $("#property_type").val();
    if (propertyType === "other") {
        $("#OtherPropertyTypeGroup").removeClass("hidden");
    } else {
        $("#OtherPropertyTypeGroup").addClass("hidden");

    }
}

function OtherUsageType() {
    var propertyType = $("#usage_type").val();
    if (propertyType === "other") {
        $("#OtherUsageType").removeClass("hidden");
    } else {
        $("#OtherUsageType").addClass("hidden");

    }
}

$(function () {


//owner detail
    var validator = new FormValidator(
    'EditOwnerDetail',
    [
        {
            name: 'owner_name',
            display: 'نام ',
            rules: 'required'
        },
        {
            name: 'owner_family',
            display: 'نام خانوادگی',
            rules: 'required'
        },
        {
            name: 'owner_cell',
            display: 'تلفن همراه',
            rules: 'required|numeric|exact_length[11]'
        },
        {
            name: 'owner_tell',
            display: 'تلفن ثابت',
            rules: 'numeric'
        }
    ],
    function (errors, event) {
        if (errors.length > 0) {
            event.preventDefault();
            var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
            for (i = 0; i < errors.length; i++) {
                result += '<li>' + errors[i].message + '</li>';
            }
            result += '</ul></div>';
            $('#EditOwnerDetailFormError').html(result);

        }
    }
    

);
//Edit PropertyDetail Form
	new FormValidator(
		'EditPropertyDetailForm',
		[
			{
				name: 'no_bedrooms',
				display: 'تعداد خواب',
				rules: 'numeric'
			},
			{
				name: 'no_floor',
				display: 'طبقه',
				rules: 'numeric'
			},
			{
				name: 'floors',
				display: 'تعداد طبقات',
				rules: 'numeric'
			},
			{
				name: 'tedade_vahed',
				display: 'تعداد واحد',
				rules: 'numeric'
			},
			{
				name: 'masahat_zamin',
				display: 'مساحت زمین',
				rules: 'numeric'
			},
			{
				name: 'masahat_kol',
				display: 'مساحت کل',
				rules: 'required|numeric'
			},
			{
				name: 'zirbana',
				display: 'زیربنا',
				rules: 'numeric'
			},
			{
				name: 'ToolBar',
				display: 'طول بر',
				rules: 'numeric'
			},
			{
				name:'address',
				display:'آدرس کامل',
				rules:'required|max_length[200]'
			}
		],
		function (errors, event) {
        if (errors.length > 0) {
            event.preventDefault();
            var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
            for (i = 0; i < errors.length; i++) {
                result += '<li>' + errors[i].message + '</li>';
            }
            result += '</ul></div>';
            $('#EditPropertyDetailFormError').html(result);

        }
    }
	);



    $("#property_type").change(function () {
        OtherPropertyTypeState();
    });
    OtherPropertyTypeState();


    $("#usage_type").change(function() {
        OtherUsageType();
    });
    OtherUsageType();
});

