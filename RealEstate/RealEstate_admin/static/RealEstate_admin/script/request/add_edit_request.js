function OtherPropertyTypeState() {
    var propertyType = $("#property_type").val();
    if (propertyType === "other") {
        $("#OtherPropertyTypeGroup").removeClass("hidden");
    } else {
        $("#OtherPropertyTypeGroup").addClass("hidden");
    }
}

 $("#property_type").change(function () {
        OtherPropertyTypeState();
    });
    OtherPropertyTypeState();

$(function () {


 var validator = new FormValidator(
      'addEditPropertyRequestForm',
      [
	 {
              name: 'phone_no',
              display: 'شماره تماس',
              rules: 'numeric'
         },
         {
              name: 'total_price',
              display: 'قیمت کل',
              rules: 'numeric'
         },
	 {
              name: 'rahne_kamel_price',
              display: 'رهن کامل',
              rules: 'numeric'
         }, 
	 {
              name: 'rahn_price',
              display: 'رهن',
              rules: 'numeric'
         },
	 {
              name: 'ejare_price',
              display: 'اجاره',
              rules: 'numeric'
          }  
      ],
      function (errors, event) {
          if (errors.length > 0) {
              event.preventDefault();
              var result = '<div class="error"><p>خطا در اطلاعات ورودی!!!</p><ul>';
              for (i = 0; i < errors.length; i++) {
                  result += '<li>' + errors[i].message + '</li>';
              }
              result += '</ul></div>';
              $('#addEditPropertyRequestFormError').html(result);

          }
      }
  );
   
});


