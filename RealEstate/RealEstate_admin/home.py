#! /usr/bin/env python
# -*- coding: utf8 -*-

from django.http import HttpResponse
from RealEstate import settings
from django.shortcuts import render
from django.contrib.auth.models import User
from RealEstate_admin.models import File,Deal,PropertyRequest,ContactUs,Profile
import datetime
from SOAPpy import WSDL
from RealEstate.SpaceRestrictorMid import SpaceCalc

def index(request):
    profiles = Profile.objects.all()
    if profiles:
        profile_id = profiles[0].id
    else:
        profile_id = None

    file_count=File.objects.all().count()
    active_deal_count=Deal.objects.filter(is_active=True).count()
    startdate = datetime.datetime.today()
    enddate = startdate + datetime.timedelta(days=15)
    end_deal_count=Deal.objects.filter(deal_end_date__range=[startdate, enddate]).count()
    active_users_count = User.objects.filter(is_active=True, is_superuser=False).count()
    property_request_count = PropertyRequest.objects.all().count()
    special_deals_count=Deal.objects.filter(is_active=True,is_special=True).count()
    #calculate remaining sms credit
    remaining_sms_credit = None
    try:
        _server = WSDL.Proxy(settings.SMS_WEB_SERVICE_URL)
        remaining_sms_credit = _server.CREDIT_LINESMS(settings.SMS_USERNAME,settings.SMS_PASSWORD,settings.SMS_SENDER_NO)
    except:
        pass
    if remaining_sms_credit == -1 or remaining_sms_credit == -2 :
        remaining_sms_credit = None
    #calculate number of matches between requests and files
    matched_file_request_count = 0
    active_deals = Deal.objects.filter(is_active=True)
    property_requests = PropertyRequest.objects.all()
    for property_request in property_requests:
        for active_deal in active_deals:
            if (property_request.deal_type == active_deal.deal_type) and \
               (property_request.total_price >= active_deal.total_price) and \
               (property_request.rahne_kamel_price >= active_deal.rahne_kamel_price) and \
                   (property_request.rahn_price >= active_deal.rahn_price) and \
               (property_request.ejare_price >= active_deal.ejare_price) and \
               (property_request.property_type == active_deal.deal_file.property_detail.property_type) :
                matched_file_request_count += 1
                break

    contact_request_count = ContactUs.objects.all().count()

    size_calc=SpaceCalc()
    space_use_size=(size_calc.get_db_size()+size_calc.get_dir_size())/1024#size in kb
    space_use_persent=(space_use_size*100)/ (int(settings.MAX_SITE_SIZE_IN_MB)*1024)

    return render(
        request,
        'RealEstate_admin/home/index.html',
        {
            "file_count":file_count,
            "active_deal_count":active_deal_count,
            "end_deal_count":end_deal_count,
            "active_users_count":active_users_count,
            "property_request_count":property_request_count,
            "matched_file_request_count":matched_file_request_count,
            "remaining_sms_credit":remaining_sms_credit,
            "contact_request_count":contact_request_count,
            "special_deals_count":special_deals_count,
            "MAX_SITE_SIZE_IN_MB":int(settings.MAX_SITE_SIZE_IN_MB)*1024 ,#in kb,
            "space_use_size":space_use_size,
            "remined_space":((int(settings.MAX_SITE_SIZE_IN_MB)*1024)-space_use_size),
            "space_use_persent":space_use_persent,
            "profile_id": profile_id
        }
    )
