#! /usr/bin/env python
# -*- coding: utf8 -*-

from django.shortcuts import render
from RealEstate_admin.models import Deal
import datetime

def get_deals(request):
	startdate = datetime.datetime.today()
	enddate = startdate + datetime.timedelta(days=15)
	end_deals=Deal.objects.filter(deal_end_date__range=[startdate, enddate])
	return render(request,'RealEstate_admin/end_deals/index.html',{'end_deals':end_deals})
