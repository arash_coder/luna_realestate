#! /usr/bin/env python
# -*- coding: utf8 -*-

"""
@author: AR.Jozaghi
@organization: LunaSoft
@copyright: Copyright © 2014 LUNA
@license:
@contact:
"""
from django.db import models
from django_resized import ResizedImageField

#-----------------------------------------------------------------------

# PropertyOption Model
class PropertyOption(models.Model):
    sarmayesh = models.CharField(max_length=500, blank=True, null=True, verbose_name="سرمایش")
    garmayesh = models.CharField(max_length=500, blank=True, null=True, verbose_name="گرمایش")
    kaf_saloon = models.CharField(max_length=500, blank=True, null=True, verbose_name="کف سالن")
    kaf_khabha = models.CharField(max_length=500, blank=True, null=True, verbose_name="کف خوابها")
    badane_divar = models.CharField(max_length=500, blank=True, null=True, verbose_name="بدنه/دیوار")
    tozihat = models.CharField(max_length=1000, blank=True, null=True, verbose_name="توضیحات")
    mahvare_markazi = models.BooleanField(verbose_name="ماهواره مرکزی")
    internet_wireless = models.BooleanField(verbose_name="اینترنت وایرلس")
    jarubarghi_markazi = models.BooleanField(verbose_name="جاروبرقی مرکزی")
    shooting_zobale = models.BooleanField(verbose_name="شوتینگ زباله")
    salon_ejtemaat = models.BooleanField(verbose_name="سالن اجتماعات")
    liting = models.BooleanField(verbose_name="لایتینگ")
    estakhr = models.BooleanField(verbose_name="استخر")
    sona = models.BooleanField(verbose_name="سونا")
    jakuzi = models.BooleanField(verbose_name="جکوزی")
    salon_varzesh = models.BooleanField(verbose_name="سالن ورزش")
    roof_garden = models.BooleanField(verbose_name="روف گاردن")
    barbecu = models.BooleanField(verbose_name="باربکیو")
    elam_harigh = models.BooleanField(verbose_name="اعلام حریق")
    etfa_harigh = models.BooleanField(verbose_name="اطفاء حریق")
    ab_taht_feshar = models.BooleanField(verbose_name="آب تحت فشار")
    kabinet = models.BooleanField(verbose_name="کابینت")
    gaaz_va_fer_tookar = models.BooleanField(verbose_name="گاز و فر توکار")
    Open = models.BooleanField(verbose_name="اوپن")
    iphone = models.BooleanField(verbose_name="آیفون")
    parde = models.BooleanField(verbose_name="پرده")
    komod = models.BooleanField(verbose_name="کمد")
    looster = models.BooleanField(verbose_name="لوستر")
    shoomine = models.BooleanField(verbose_name="شومینه")
    master = models.BooleanField(verbose_name="مستر")
    kabin_doosh = models.BooleanField(verbose_name="کابین دوش")
    noorpardazi = models.BooleanField(verbose_name="نورپردازی")
    noorgir_pasiyo = models.BooleanField(verbose_name="نورگیر/پاسیو")
    tras = models.BooleanField(verbose_name="تراس")
    anbari = models.BooleanField(verbose_name="انباری")
    parking = models.BooleanField(verbose_name="پارکینگ")
    asansor = models.BooleanField(verbose_name="آسانسور")
    darb_automatik = models.BooleanField(verbose_name="درب اتوماتیک")
    vorudi_zed_serghat = models.BooleanField(verbose_name="ورودی ضد سرقت")
    hayate_ekhtesasi = models.BooleanField(verbose_name="حیات اختصاصی")
    labi = models.BooleanField(verbose_name="لابی")

    is_dirty = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    desktop_id = models.IntegerField(null=True)
#-----------------------------------------------------------------------

# PropertyDetail Model
class PropertyDetail(models.Model):
    #property type
    PROPERTY_TYPE = (
        ('apartment', 'آپارتمان ‍‍‍‍‍‍‍'),
        ('house', 'خانه'),
        ('suite', 'سوئیت'),
        ('shop', 'مغازه'),
        ('other', 'سایر')
    )
    property_type = models.CharField(max_length=20, choices=PROPERTY_TYPE, verbose_name="نوع ملک")
    other_property_type_name = models.CharField(max_length=200, blank=True, null=True, verbose_name="نوع ملک")
    #usage type
    USAGE_TYPE = (
        ('maskuny', 'سند مسکونی'),
        ('maskuny_tejari', 'مسکونی تجاری'),
        ('other', 'سایر')
    )
    usage_type = models.CharField(max_length=20, choices=USAGE_TYPE, verbose_name="نوع کاربری")
    other_usage_type_name = models.CharField(max_length=200, blank=True, null=True, verbose_name="نوع کاربری")
    no_bedrooms = models.IntegerField(null=True, verbose_name="تعداد خواب")
    no_floor = models.CharField(max_length=50, null=True, blank=True, verbose_name="تعداد طبقات")
    floors = models.CharField(max_length=50, null=True, blank=True, verbose_name="طبقه")
    tedade_vahed = models.CharField(max_length=50, null=True, blank=True, verbose_name="تعداد واحد")
    area = models.CharField(max_length=500, blank=True, null=True, verbose_name="محدوده")
    address = models.CharField(max_length=200, blank=True, null=True, verbose_name="آدرس")
    masahat_zamin = models.BigIntegerField(null=True, verbose_name="مساحت زمین")
    masahat_kol = models.BigIntegerField(null=True, verbose_name="مساحت کل")
    zirbana = models.BigIntegerField(null=True, verbose_name="زیربنا")
    ToolBar = models.BigIntegerField(null=True, verbose_name="طول بر")
    zabete_sakht = models.CharField(max_length=100, blank=True, null=True, verbose_name="ضابطه ساخت")
    sene_bana = models.IntegerField(null=True, verbose_name="سن بنا")
    mogheiyat = models.CharField(max_length=100, blank=True, null=True, verbose_name="موقعیت")
    noe_sanad = models.CharField(max_length=500, blank=True, null=True, verbose_name="نوع سند")
    vame_banki = models.CharField(max_length=500, blank=True, null=True, verbose_name="وام بانکی")
    noe_saghf = models.CharField(max_length=500, blank=True, null=True, verbose_name="نوع سقف")
    nama = models.CharField(max_length=500, blank=True, null=True, verbose_name="نما")
    view_ya_manzare = models.CharField(max_length=500, blank=True, null=True, verbose_name="ویو/منظره")

    is_dirty = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    desktop_id = models.IntegerField(null=True)
#-----------------------------------------------------------------------
#File Model
class File(models.Model):
    code = models.IntegerField(default=0, blank=True)
    code_type = models.CharField(max_length=1, default='w')  # W for Website, D for Desktop, and M for Mobile
    creation_date = models.DateTimeField(auto_now_add=True)

    @property
    def file_code(self):
        return str(self.code) + self.code_type
    #owner data
    owner_name = models.CharField(max_length=200, blank=True, null=True)
    owner_family = models.CharField(max_length=200, blank=True)
    owner_tell = models.CharField(max_length=20)
    owner_cell = models.CharField(max_length=20)

    property_detail = models.OneToOneField(PropertyDetail, blank=True, null=True)
    property_option = models.OneToOneField(PropertyOption, blank=True, null=True)

    is_dirty = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    desktop_id = models.IntegerField(null=True)

    def __unicode__(self):
        return u'کد فایل : {0} | نام و نام خانوادگی مالک : {1} {2} | شماره همراه مالک : {3}'.format(self.file_code,
                                                                                                       self.owner_name,
                                                                                                       self.owner_family,
                                                                                                       self.owner_cell)


#-----------------------------------------------------------------------
# Deal Model
class Deal(models.Model):
    DEAL_TYPE = (
        ('rahn_va_ejare', u'رهن و اجاره'),
        ('rahn_kamel', u'رهن کامل'),
        ('forush_malekiyat', u'فروش مالکیت'),
        ('forush_sarghofly', u'فروش سرقفلی'),
        ('moaveze', u'معاوضه'),
        ('pish_forush', u'پیش فروش'),
        ('mosharekat_dar_sakht', u'مشارکت در ساخت')
    )
    deal_type = models.CharField(max_length=20, choices=DEAL_TYPE)
    BAZDID_TYPE = (
        ('takhliye', u'تخلیه'),
        ('negahban', u'نگهبان'),
        ('sokunat_malek', u'سکونت مالک '),
        ('sokunat_mostajer', u'سکونت مستاجر'),
        ('klid_bongah', u'کلید بنگاه')
    )
    bazdid_type = models.CharField(max_length=20, choices=BAZDID_TYPE)
    name_mostajer_ya_negahban = models.CharField(max_length=200, blank=True, null=True)
    tell_mostajer_ya_negahban = models.CharField(max_length=20, blank=True, null=True)
    tarikhe_takhliye = models.DateTimeField(null=True)
    total_price = models.BigIntegerField(null=True)
    meter_price = models.BigIntegerField(null=True)
    rahne_kamel_price = models.BigIntegerField(null=True)
    rahn_price = models.BigIntegerField(null=True)
    ejare_price = models.BigIntegerField(null=True)
    charge_price = models.BigIntegerField(null=True)
    ghabel_tabdil = models.CharField(max_length=200, blank=True, null=True)
    is_active = models.BooleanField()
    is_special = models.BooleanField(default=False)
    deal_file = models.ForeignKey(File)

    deal_creation_date = models.DateTimeField(auto_now_add=True)
    deal_end_date = models.DateTimeField(null=True)
    deal_name = models.CharField(max_length=100, blank=True, null=True)
    deal_family = models.CharField(max_length=100, blank=True, null=True)
    deal_cell = models.CharField(max_length=100, blank=True, null=True)
    deal_description = models.CharField(max_length=300, blank=True, null=True)
    is_synced = models.BooleanField(default=True)

    is_dirty = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    desktop_id = models.IntegerField(null=True)

#-----------------------------------------------------------------------

# Pic Model
class Picture(models.Model):
    pic = ResizedImageField(max_width=700, max_height=500, upload_to='documents')
    pic_file = models.ForeignKey(File)
    caption = models.CharField(max_length=100, blank=True, null=True)

#-----------------------------------------------------------------------

class PropertyRequest(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    family = models.CharField(max_length=100, blank=True, null=True)
    phone_no = models.CharField(max_length=20, blank=True, null=True)
    total_price = models.BigIntegerField(null=True)
    rahne_kamel_price = models.BigIntegerField(null=True)
    rahn_price = models.BigIntegerField(null=True)
    ejare_price = models.BigIntegerField(null=True)
    #property type
    PROPERTY_TYPE = (
        ('apartment', 'آپارتمان ‍‍‍‍‍‍‍'),
        ('house', 'خانه'),
        ('suite', 'سوئیت'),
        ('shop', 'مغازه'),
        ('other', 'سایر')
    )
    property_type = models.CharField(max_length=20, choices=PROPERTY_TYPE, verbose_name="نوع ملک")
    other_property_type_name = models.CharField(max_length=200, blank=True, null=True, verbose_name="نوع ملک")
    #deal type
    DEAL_TYPE = (
        ('rahn_va_ejare', u'رهن و اجاره'),
        ('rahn_kamel', u'رهن کامل'),
        ('forush_malekiyat', u'فروش مالکیت'),
        ('forush_sarghofly', u'فروش سرقفلی'),
        ('moaveze', u'معاوضه'),
        ('pish_forush', u'پیش فروش'),
        ('mosharekat_dar_sakht', u'مشارکت در ساخت')
    )
    deal_type = models.CharField(max_length=20, choices=DEAL_TYPE)
    creation_date = models.DateTimeField(auto_now_add=True)
    area = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)

    is_dirty = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)

    desktop_id = models.IntegerField(null=True)
#-----------------------------------------------------------------------
class ContactUs(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    main_text = models.CharField(max_length=1000)
    name = models.CharField(max_length=100, blank=True, null=True)
    phone_no = models.CharField(max_length=20, blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)


class Profile(models.Model):
    phone_no = models.CharField(max_length=11, blank=True, null=True)
    mobile_no = models.CharField(max_length=11, blank=True, null=True)
    about_us = models.CharField(max_length=10000, blank=True, null=True)
    about_us_title = models.CharField(max_length=100, blank=True, null=True)
    pic1 = ResizedImageField(max_width=570, max_height=300, upload_to='documents', blank=True, null=True)
    pic2 = ResizedImageField(max_width=570, max_height=300, upload_to='documents', blank=True, null=True)
    pic3 = ResizedImageField(max_width=570, max_height=300, upload_to='documents', blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
